function Switch-Account {
    param (
        [string]$Title = 'Select Role and Account to assume'
    )

    Write-Host "Inialising IAM profile..."
    #Set-AWSCredential -StoreAs IAM_profile -AccessKey <access_key_id> -SecretKey <secret_key>

    Write-Host "================ $Title ================"
    $roles = Import-Csv "C:\AWS\roles.csv"
    $i = 0
    foreach ($line in $roles) {
        if($roles.role[$i] -like "*Administrator*"){
            Write-Host -ForegroundColor Red "$i - $($roles.account[$i])/role/$($roles.role[$i])"
            $i+=1
        }
        else {
            Write-Host -ForegroundColor Yellow "$i - $($roles.account[$i])/role/$($roles.role[$i])"
            $i+=1
        }
    }
    Write-Host -ForegroundColor Cyan "q - Quit"
    
    $selection = Read-Host "Please make a selection"
    switch ($selection)
    {
        $selection {
            if ($selection -eq "q"){
                return
            }
            if($roles.role[$selection] -like "*Administrator*"){
                Write-Host -ForegroundColor Red "Assuming role to $($roles.account[$selection])/$($roles.role[$selection])"
            }
            else{
                Write-Host -ForegroundColor Yellow "Assuming role to $($roles.account[$selection])/$($roles.role[$selection])"
            }
            #Set-AWSCredential -StoreAS Role_profile -SourceProfile IAM_profile -RoleArn "arn:aws:iam::$($roles.account[$selection]):role/$($roles.role[$selection])"
            #Get-AWSCredential -ProfileName Role_profile
        }
    }
}

Switch-Account